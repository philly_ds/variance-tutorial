from abc import ABC, abstractmethod


class IOnlineVarianceCalculator(ABC):
    @abstractmethod
    def consume(self, value: float):
        """
        Take in one value and add it to the variance calculation.
        :param value: One floating point number.
        """
        pass

    @abstractmethod
    def variance(self) -> float:
        """Calculate the variance given all values consumed."""
        pass
