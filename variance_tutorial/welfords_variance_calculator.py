from variance_tutorial.i_online_variance_calculator import IOnlineVarianceCalculator
from math import sqrt


class WelfordsVarianceCalculator(IOnlineVarianceCalculator):
    """
    Numerically stable method for calculating mean and standard deviation
    """

    def __init__(self):
        self.__mean = 0.0
        self.__m2 = 0.0  # Sum of squares of differences from the current mean
        self.__count = 0

    def consume(self, value: float):
        self.__count = self.__count + 1
        delta = value - self.__mean
        self.__mean = self.__mean + delta / self.__count
        delta2 = value - self.__mean
        self.__m2 = self.__m2 + (delta * delta2)

    def variance(self) -> float:
        if self.__count < 2:
            raise ArithmeticError("Insufficiently large number of values to calculate variance.")

        variance = self.__m2 / (self.__count - 1)
        return variance
