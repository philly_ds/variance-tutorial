from variance_tutorial.i_online_variance_calculator import IOnlineVarianceCalculator


class NaiveVarianceCalculator(IOnlineVarianceCalculator):
    def __init__(self):
        self.__n = 0
        self.__sum = 0.0
        self.__sum_sq = 0.0

    def consume(self, value: float):
        self.__n = self.__n + 1
        self.__sum = self.__sum + value
        self.__sum_sq = self.__sum_sq + (value * value)

    def variance(self) -> float:
        if self.__n < 3:
            raise ArithmeticError("Insufficiently large number of values to calculate variance.")
        return (self.__sum_sq - (self.__sum * self.__sum) / self.__n) / (self.__n - 1)
