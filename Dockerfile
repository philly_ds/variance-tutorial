FROM python:3.6-alpine

WORKDIR /tmp/variance-tutorial/

RUN pip install pipenv
COPY . .
RUN pipenv sync --dev
RUN ls -all tests
RUN eval $(pipenv --py) -m pytest