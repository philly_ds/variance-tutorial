Code from the June 7th Data Science Community Call presentation. Demonstrates the value of managed environments, unit tests, and code reposities.

[![Build Status](http://18.217.238.147/buildStatus/icon?job=variance-tutorial/master)](http://18.217.238.147/blue/organizations/jenkins/variance-tutorial/activity?branch=master)

Build locally so you have a image:
```bash
docker build -t variance-tutorial:latest .
```

List your docker images
```bash
docker images 
```

Remove dangling images
```bash
docker rmi --force $(docker images -q -f dangling=true)
```

Run as docker container locally
```bash
docker run -it -d -p <hostPort>:<containerPort> variance-tutorial:latest
```

Example of running locally forwarded to localhost:8081 while the container is listening to port 8080
```bash
docker run -it -d -p 8081:8080 variance-tutorial:latest
```

List running docker containers
```bash
docker ps
```

Stop running container
```bash
docker rm --force <container-id>
```
