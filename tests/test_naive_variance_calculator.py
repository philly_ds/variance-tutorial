import pytest
from variance_tutorial.naive_variance_calculator import NaiveVarianceCalculator


epsilon = 1e-8


def test_too_few_numbers_throw_error():
    values = [4.0]
    with pytest.raises(ArithmeticError):
        pass_values_to_calculator(values)


def test_return_expected_variance():
    values = [4.0, 7.0, 13.0, 16.0]
    calculated = pass_values_to_calculator(values)
    assert 30.0 == pytest.approx(calculated, epsilon)


def test_return_expected_variance_high_numbers():
    values = [10e8 + 4.0, 10e8 + 7.0, 10e8 + 13.0, 10e8 + 16.0]
    calculated = pass_values_to_calculator(values)
    assert 30.0 == pytest.approx(calculated, epsilon)


def pass_values_to_calculator(values: list) -> float:
    variance = NaiveVarianceCalculator()
    for value in values:
        variance.consume(value)
    return variance.variance()
